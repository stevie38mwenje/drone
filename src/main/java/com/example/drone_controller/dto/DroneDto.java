package com.example.drone_controller.dto;

import com.example.drone_controller.domain.DroneModelEnum;
import com.example.drone_controller.domain.StateEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DroneDto {
    private String serialNumber;
    @Enumerated(EnumType.STRING)
    private DroneModelEnum droneModelEnum;
    private Double weightLimit;
    private Double batteryCapacity;
    @Enumerated(EnumType.STRING)
    private StateEnum stateEnum;
}

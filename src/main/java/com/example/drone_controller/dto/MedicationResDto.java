package com.example.drone_controller.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Data
@Slf4j
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MedicationResDto {
    private UUID medicationId;
    private String name;
    private int weight;
    private String code;
    private UUID id;
    private UUID imageId;
}

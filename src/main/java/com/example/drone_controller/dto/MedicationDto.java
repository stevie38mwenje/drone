package com.example.drone_controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicationDto {
    private String name;
    private Double weight;
    private String code;
    private UUID id;
}

package com.example.drone_controller.domain;

public enum DroneModelEnum {
    Lightweight, Middleweight, Cruiserweight, Heavyweight
}

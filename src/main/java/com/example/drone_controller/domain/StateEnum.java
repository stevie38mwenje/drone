package com.example.drone_controller.domain;

public enum StateEnum {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}

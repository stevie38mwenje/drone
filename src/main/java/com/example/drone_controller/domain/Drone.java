package com.example.drone_controller.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
@Entity

public class Drone{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100, unique = true)
    private String serialNumber;

    @Column(name = "drone_uid", updatable = false, nullable = false, length = 32, columnDefinition = "uuid")
    private UUID uid = UUID.randomUUID();
    @Enumerated(EnumType.STRING)
    private DroneModelEnum droneModelEnum;
    @DecimalMax("500.0")
    private Double weightLimit;
    private Double batteryCapacity;
    @Enumerated(EnumType.STRING)
    private StateEnum stateEnum;
    @JsonIgnore
    @OneToMany(mappedBy ="drone" ,fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Medication> medicationList;


}

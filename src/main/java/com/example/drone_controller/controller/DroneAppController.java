package com.example.drone_controller.controller;

import com.example.drone_controller.domain.Drone;
import com.example.drone_controller.dto.DroneDto;
import com.example.drone_controller.domain.Medication;
import com.example.drone_controller.dto.MedicationDto;
import com.example.drone_controller.service.DroneService;
import com.example.drone_controller.service.MedicationService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

@RestController
@Log4j2
@AllArgsConstructor
@RequestMapping("drone")
public class DroneAppController {
    private final DroneService droneService;
    private final MedicationService medicationService;


    @PostMapping("/register")
    public ResponseEntity<?> addDrone(@RequestBody DroneDto drone) {
        var new_drone = droneService.registerDrone(drone);
        return ResponseEntity.status(HttpStatus.CREATED).body(new_drone);
    }

    @PostMapping(value = "addMedication/{droneId}")
    Drone addMedication(@PathVariable Drone drone, @RequestBody @Valid MedicationDto medicationDto) throws Exception {
        return droneService.addMedicationItemsList(drone,medicationDto);
    }




    @PostMapping(path = "/add-medication", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> addMedication(@RequestPart("file") MultipartFile file, @RequestPart("medication") String medication) throws IOException
    {
        if (file == null) throw new FileNotFoundException("document not found");
        byte[] bytes = file.getBytes();

        MedicationDto medicationDto = medicationService.getJson(medication);

        return ResponseEntity.ok().body(medicationService.addMedication(medicationDto, bytes));
    }


//    @GetMapping(value = "get-medication/{droneId}")
//    List<Medication> getMedication(@PathVariable(name = "droneId") Long id)
//    {
//        return droneService.getLoadedMedications(id);
//    }

    @GetMapping(value = "available")
    List<Drone> getAvailable()
    {
        return droneService.getAvailableDrones();
    }
    @GetMapping
    List<Drone> getAll()
    {
        return droneService.getAllDrones();
    }

    @PutMapping("update/{droneId}")
    Drone update(@PathVariable Long uid, @RequestBody Drone drone)
    {
        return droneService.editDrone(uid, drone);
    }

    @GetMapping("battery/{droneId}")
    Double getBatteryCapacity(@PathVariable Long uid)
    {
        return droneService.getBatteryCapacity(uid);
    }

}

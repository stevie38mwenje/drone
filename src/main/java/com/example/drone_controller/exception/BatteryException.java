package com.example.drone_controller.exception;

public class BatteryException extends RuntimeException{
    public BatteryException() {
        super();
    }

    public BatteryException(String message) {
        super(message);
    }
}

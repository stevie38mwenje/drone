package com.example.drone_controller.exception;

public class DroneNotFoundException extends RuntimeException{
    public DroneNotFoundException() {
        super();
    }

    public DroneNotFoundException(String message) {
        super(message);
    }
}

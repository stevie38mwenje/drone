package com.example.drone_controller.exception;

public class WeightException extends RuntimeException{
    public WeightException() {
        super();
    }

    public WeightException(String message) {
        super(message);
    }
}

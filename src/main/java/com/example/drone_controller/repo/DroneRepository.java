package com.example.drone_controller.repo;

import com.example.drone_controller.domain.Drone;
import com.example.drone_controller.domain.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface DroneRepository extends JpaRepository<Drone,Long> {

    Drone findByUid(UUID uid);
}

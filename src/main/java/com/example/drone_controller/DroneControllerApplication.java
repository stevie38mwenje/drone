package com.example.drone_controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroneControllerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DroneControllerApplication.class, args);
    }

}

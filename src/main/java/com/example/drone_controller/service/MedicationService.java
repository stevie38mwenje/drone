package com.example.drone_controller.service;

import com.example.drone_controller.domain.Medication;
import com.example.drone_controller.dto.MedicationDto;

public interface MedicationService {
    MedicationDto getJson(String medication);

    MedicationDto addMedication(MedicationDto medicationDto, byte[] bytes);
}

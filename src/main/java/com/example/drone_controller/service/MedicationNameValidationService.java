package com.example.drone_controller.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class MedicationNameValidationService {
    private final static Logger logger = LoggerFactory.getLogger(MedicationNameValidationService.class);

    public final String NAME_REGEX_PATTERN = "^[a-zA-Z0-9_-]*$";

    public boolean validateName(String name) {
        logger.info("Validating name: {}", name);

        return Pattern.compile(NAME_REGEX_PATTERN)
                .matcher(name)
                .matches();
    }
}

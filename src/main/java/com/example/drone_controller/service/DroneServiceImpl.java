package com.example.drone_controller.service;

import com.example.drone_controller.domain.*;
import com.example.drone_controller.dto.DroneDto;
import com.example.drone_controller.dto.MedicationDto;
import com.example.drone_controller.exception.BatteryException;
import com.example.drone_controller.exception.DroneNotFoundException;
import com.example.drone_controller.exception.WeightException;
import com.example.drone_controller.repo.DroneRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService{
    private final DroneRepository droneRepository;
    private final MedicationNameValidationService nameValidationService;
    private final MedicationCodeValidationService codeValidationService;
    private final Response response;
    private final static Logger logger = LoggerFactory.getLogger(DroneServiceImpl.class);


    @Override
    public Drone registerDrone(DroneDto droneDto) {
        logger.info("Persisting drone data: {}", droneDto);
        Drone drone = new Drone();
        drone.setStateEnum(StateEnum.IDLE);
        drone.setDroneModelEnum(droneDto.getDroneModelEnum());
        drone.setSerialNumber(droneDto.getSerialNumber());
        drone.setWeightLimit(droneDto.getWeightLimit());
        drone.setBatteryCapacity(droneDto.getBatteryCapacity());
        return droneRepository.save(drone);
    }

    @Override
    public Drone addMedicationItemsList(Drone drone, MedicationDto medication) {
//        var newMedication = genericMapper2.mapForward(medication, MedicationEntity.class);
        if (drone.getStateEnum().equals(StateEnum.IDLE) || drone.getStateEnum().equals(StateEnum.LOADING)) {
            var weight = drone.getWeightLimit();
            weight += medication.getWeight();
            logger.info("Weight: {}", weight);
            if (weight > 500) {
                drone.setStateEnum(StateEnum.LOADED);
                throw new WeightException("The drone will exceed weight limit. Please Reduce the load!");
            }
            drone.setStateEnum(StateEnum.LOADING);
            drone.setWeightLimit(weight);
            var batteryCapacity = drone.getBatteryCapacity();
            if (batteryCapacity < 25) {
                drone.setStateEnum(StateEnum.LOADED);
                throw new BatteryException("Battery is below 25%!!!");
            }
            drone.setStateEnum(StateEnum.LOADING);
            //Assume that each medication will consume 10 % of the battery
            batteryCapacity -= 10;
            drone.setBatteryCapacity(batteryCapacity);
//            var medications = drone.getMedicationList();
//            newMedication.
//            medicationEntities.add(medicationEntity);
//
//            drone.setItems(medicationEntities);
//
//            return drone;

        }
        throw  new DroneNotFoundException("Drone not found.");

    }

//    @Override
//    public List<Medication> getLoadedMedications(Long uid) {
//        var drone= droneRepository.findById(uid);
//        if(drone.isPresent()){
//            var medications = droneRepository.findLoadedMedications(uid);
//            return medications;
//        }
//        else throw new DroneNotFoundException("Drone not found") ;
//    }

    @Override
    public List<Drone> getAvailableDrones() {
        var allDrones = droneRepository.findAll();
        List<Drone> droneList = new ArrayList<>();
        for(Drone drone:allDrones){
            if(drone.getStateEnum().equals(StateEnum.IDLE)){
                droneList.add(drone);
            }
        }
        return droneList;
    }

    @Override
    public List<Drone> getAllDrones() {
        return droneRepository.findAll();
    }

    @Override
    public Drone editDrone(Long droneId, Drone drone) {
        return null;
    }

    @Override
    public Double getBatteryCapacity(Long droneId) {
        var drone = droneRepository.findById(droneId);
        if(drone.isPresent()){
            var batteryCapacity = drone.get().getBatteryCapacity();
            return batteryCapacity;
        }
        else throw new DroneNotFoundException("Drone not found");
    }
}

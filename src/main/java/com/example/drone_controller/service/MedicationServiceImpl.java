package com.example.drone_controller.service;

import com.example.drone_controller.domain.Medication;
import com.example.drone_controller.domain.Response;
import com.example.drone_controller.domain.StateEnum;
import com.example.drone_controller.dto.MedicationDto;
import com.example.drone_controller.dto.MedicationResDto;
import com.example.drone_controller.exception.BadRequestException;
import com.example.drone_controller.exception.BatteryException;
import com.example.drone_controller.exception.DroneNotFoundException;
import com.example.drone_controller.exception.WeightException;
import com.example.drone_controller.repo.DroneRepository;
import com.example.drone_controller.repo.MedicationRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MedicationServiceImpl implements MedicationService{
    private final DroneRepository droneRepository;
    private final MedicationRepository medicationRepository;
    private final MedicationNameValidationService nameValidationService;
    private final MedicationCodeValidationService codeValidationService;
    private final static Logger logger = LoggerFactory.getLogger(MedicationServiceImpl.class);
    private final MedicationResDto medicationResDto;
    private final Response response;

    @Override
    public MedicationDto getJson(String medication) {
        var medicationDto = new MedicationDto();
        try
        {
            var objectMapper = new ObjectMapper();
            medicationDto = objectMapper.readValue(medication,MedicationDto.class);
            logger.info("Medication data: {}", medicationDto);

        } catch (JsonProcessingException e) {
            throw new BadRequestException("An error occurred while processing medication");
        }
        return medicationDto;
    }

    @Override
    public MedicationDto addMedication(MedicationDto medicationDto, byte[] image) {
        var medication = new Medication();
        var medicationCode = medicationDto.getCode();
        var medicationName = medicationDto.getName();

        var drone = droneRepository.findByUid(medicationDto.getId());
        if (drone == null) {
            throw new DroneNotFoundException("Drone does not exist");
        }
        var batteryCapacity = drone.getBatteryCapacity();

        logger.info("Medication dto: {}", medicationDto);
        logger.info("Drone object: {}", drone);

        if (drone.getStateEnum().equals(StateEnum.IDLE) || drone.getStateEnum().equals(StateEnum.LOADING)) {

            if (!codeValidationService.validateCode(medicationCode)) {
                throw new BadRequestException("Only upper case letters, underscore and numbers are allowed for code");
            }
            if (!nameValidationService.validateName(medicationName)) {
                throw new BadRequestException("Only letters, numbers, underscore and dash are allowed for name");
            }
            var weight = drone.getWeightLimit() + medicationDto.getWeight();
            logger.info("Drone weight: {}", weight);

            if (weight > drone.getWeightLimit()||medicationDto.getWeight() > drone.getWeightLimit()) {
                drone.setStateEnum(StateEnum.LOADED);
                throw new WeightException("Maximum weight capacity for the drone has been reached");
            }
            if (batteryCapacity < 25) {
                drone.setStateEnum(StateEnum.LOADED);
                throw new BatteryException("Battery is below 25%!!!");
            }
            drone.setStateEnum(StateEnum.LOADING);
            drone.setWeightLimit(weight);
            drone = droneRepository.save(drone);

            medication.setDrone(drone);
            medication.setImage(String.valueOf(image));
            medication = medicationRepository.save(medication);

            medicationResDto.setMedicationId(medication.getUid());
            medicationResDto.setId(medication.getDrone().getUid());
            medicationResDto.setImageId(medication.getImg_uid());
            response.setResponseMessage(String.valueOf(medicationResDto));
            response.setResponseCode(HttpStatus.CREATED.value());

        }
        return medicationDto;
    }
}

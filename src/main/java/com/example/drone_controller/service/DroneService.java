package com.example.drone_controller.service;

import com.example.drone_controller.domain.Drone;
import com.example.drone_controller.dto.DroneDto;
import com.example.drone_controller.domain.Medication;
import com.example.drone_controller.dto.MedicationDto;

import java.util.List;

public interface DroneService {
    Drone registerDrone(DroneDto drone);

    Drone addMedicationItemsList(Drone drone, MedicationDto medicationDto);

//    List<Medication> getLoadedMedications(Long id);

    List<Drone> getAvailableDrones();

    List<Drone> getAllDrones();

    Drone editDrone(Long droneId, Drone drone);

    Double getBatteryCapacity(Long droneId);
}

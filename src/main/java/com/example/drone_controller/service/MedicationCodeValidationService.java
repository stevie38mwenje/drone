package com.example.drone_controller.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class MedicationCodeValidationService {
    private final static Logger logger = LoggerFactory.getLogger(MedicationCodeValidationService.class);

    public final String CODE_REGEX_PATTERN = "^[A-Z0-9_]*$";

    public boolean validateCode(String code) {
        logger.info("Validating code: {}", code);

        return Pattern.compile(CODE_REGEX_PATTERN)
                .matcher(code)
                .matches();
    }

}

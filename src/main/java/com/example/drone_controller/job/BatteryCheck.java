package com.example.drone_controller.job;


import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "PT7H30M")
public class BatteryCheck {
    private final static Logger logger = LoggerFactory.getLogger(BatteryCheck.class);

}

FROM openjdk:11-jre-slim
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE

COPY target/drone-0.0.1-SNAPSHOT.jar app.jar

ENV JAVA_OPTS=""

ENTRYPOINT exec java $JAVA_OPTS \
 -Djava.security.egd=file:/dev/./urandom \
 -Djdk.tls.client.protocols=TLSv1.2 \
 -Dspring.profiles.active=$SPRING_PROFILE \
# -javaagent:applicationinsights-agent-3.1.1.jar \
 -jar app.jar